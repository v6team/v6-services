'use strict';

let argv = require('minimist')(process.argv.slice(2)),
    fs = require('fs'),
    ApiServer = require('./server.js'),
    apiConf,
    apiServer,
    confPath = argv['conf'];

if (confPath){
    fs.exists(confPath, (exists) => {
        if (exists) {
            console.log(`start with conf file ${confPath}`);
            apiConf = require(confPath);
            apiServer = new ApiServer(apiConf);
            apiServer.start();
        } else {
            console.log(`conf file ${confPath} not exists`);
        }
    });
} else {
    apiConf = require('./conf.js');
    apiServer = new ApiServer(apiConf);
    apiServer.start();
}