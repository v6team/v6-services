module.exports = {
    port: 8081,
    allowOrigin: true,
    develop: true,
    logger: {
    },
    mongoStorage: {
        default: {
            host: 'localhost',
            port: 27017
        },
        games: {
            test1: {},
            test2: {}
        }
    }
};
